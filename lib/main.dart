import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sqlite_basic/pages/about_apisit.dart';
import 'package:sqlite_basic/pages/about_mantita.dart';
import 'package:sqlite_basic/pages/add_profile.dart';
import 'package:sqlite_basic/pages/edit_profile.dart';
import 'package:sqlite_basic/model/profile_model.dart';
import 'package:sqlite_basic/pages/show_profile.dart';
import 'database/database_helper.dart';
import 'pages/edit_profile.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Location',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: const MyHomePage(title: 'Travel Lits'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int? selectedId;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),

      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.indigo,
        child: Icon(Icons.add),
        onPressed: () async {
          await Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => AddProfile()),
          );
        },
      ),
      body: Center(
        child: FutureBuilder<List<ProfileModel>>(
            future: DatabaseHelper.instance.getGroceries(),
            builder: (BuildContext context,
                AsyncSnapshot<List<ProfileModel>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('Loading...'));
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text('No Profiles in List.'))
                  : ListView(
                      children: snapshot.data!.map((grocery) {
                        return Container(
                            child: Card(
                          color: selectedId == grocery.id
                              ? Colors.white70
                              : Colors.white,
                          child: Stack(children: [
                            Container(


                            ),
                            // Text('${grocery.firstname} ${grocery.lastname}'),
                            //  Text(grocery.email),

                            Image(
                              height: 400,
                              width: 400,
                              fit: BoxFit.cover,
                              image: FileImage(File(grocery.image)),
                            ),

                            Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  new IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: Icon(Icons.edit),
                                    onPressed: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                EditProfile(grocery)),
                                      ).then((value) {
                                        setState(() {});
                                      });
                                    },
                                  ),
                                  new IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: Icon(Icons.clear),
                                    onPressed: () {
                                      showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return AlertDialog(
                                            title: new Text(
                                                "ลบข้อมูล"),
                                            // content: new Text("Please Confirm"),
                                            actions: [
                                              new TextButton(
                                                onPressed: () {
                                                  DatabaseHelper.instance
                                                      .remove(grocery.id!);
                                                  setState(() {
                                                    Navigator.of(context).pop();
                                                  });
                                                },
                                                child: new Text("Ok"),
                                              ),
                                              Visibility(
                                                visible: true,
                                                child: new TextButton(
                                                  onPressed: () {
                                                    Navigator.of(context).pop();
                                                  },
                                                  child: new Text("Cancel"),
                                                ),
                                              ),
                                            ],
                                          );
                                        },
                                      );
                                    },
                                  ),
                                  new IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: Icon(Icons.description),
                                    onPressed: () {
                                      var profileid = grocery.id;
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  ShowProfile(id: profileid)));

                                      setState(() {
                                        print(grocery.image);
                                        if (selectedId == null) {
                                          //firstname.text = grocery.firstname;
                                          selectedId = grocery.id;
                                        } else {
                                          // textController.text = '';
                                          selectedId = null;
                                        }
                                      });
                                    },
                                  ),
                                ]),

                          ]),
                        ));
                      }).toList(),
                    );
            }),
      ),
      drawer: Drawer(
        child: ListView(padding: const EdgeInsets.all(0.0), children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: const Text('Menu About',style: TextStyle(
              fontSize: 25
            ),),
            accountEmail: const Text('About up'),

          ),
          ListTile(
              title: const Text('Apisit Madhadam'),
              leading: const Icon(Icons.account_circle_sharp),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder:  (context) => const AboutApisit()));
              }),
          ListTile(
              title: const Text('Mantita Rabeadee'),
              leading: const Icon(Icons.account_circle_sharp),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder:  (context) => const AboutMantita()));
              }),
          const Divider(),
          ListTile(
              title: const Text('Close'),
              leading: const Icon(Icons.close),
              onTap: () {
                Navigator.of(context).pop();
              }),
        ]),
      ),

    );
  }
}
