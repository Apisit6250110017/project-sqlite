import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqlite_basic/model/profile_model.dart';

class DatabaseHelper {
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database? _database;
  Future<Database> get database async => _database ??= await _initDatabase();

  Future<Database> _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'db_profile.db');
    return await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
    );
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE profileDB (
          id INTEGER PRIMARY KEY,
          firstname TEXT,
          lastname TEXT,
          email TEXT,
          phone TEXT,
          image TEXT
      )
      ''');
  }

  Future<List<ProfileModel>> getGroceries() async {
    Database db = await instance.database;
    var groceries = await db.query('profileDB', orderBy: 'firstname');
    List<ProfileModel> groceryList = groceries.isNotEmpty
        ? groceries.map((c) => ProfileModel.fromMap(c)).toList()
        : [];
    return groceryList;
  }

  Future<List<ProfileModel>> getProfile(int id) async{
    Database db = await instance.database;
    var profile = await db.query('profileDB', where: 'id = ?', whereArgs: [id]);
    List<ProfileModel> profileList = profile.isNotEmpty
        ? profile.map((c) => ProfileModel.fromMap(c)).toList()
        : [];
    return profileList;
  }

  Future<int> add(ProfileModel grocery) async {
    Database db = await instance.database;
    return await db.insert('profileDB', grocery.toMap());
  }

  Future<int> remove(int id) async {
    Database db = await instance.database;
    return await db.delete('profileDB', where: 'id = ?', whereArgs: [id]);
  }

  Future<int> update(ProfileModel grocery) async {
    Database db = await instance.database;
    return await db.update('profileDB', grocery.toMap(),
        where: "id = ?", whereArgs: [grocery.id]);
  }
}