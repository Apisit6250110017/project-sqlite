
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sqlite_basic/database/database_helper.dart';
import 'package:sqlite_basic/model/profile_model.dart';

class ShowProfile extends StatelessWidget {
  final id;
  ShowProfile({Key? key, required this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Detail Travel'),
      ),
      backgroundColor: Colors.grey,
      body: Center(
        child: FutureBuilder<List<ProfileModel>>(
            future: DatabaseHelper.instance.getProfile(this.id),
            builder: (BuildContext context,
                AsyncSnapshot<List<ProfileModel>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('Loading...'));
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text('No Groceries in List.'))
                  :  ListView(
                children: snapshot.data!.map((profile) {
                    return Center(
                      child: Stack(
                        children: [
                          Image(
                            width: 400,
                            height: 800,
                            fit: BoxFit.cover,

                            image: FileImage(File(profile.image)),


                          ),
                          // CircleAvatar(
                          //   backgroundImage: FileImage(File(profile.image)),
                          //   radius: 100,
                          // ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20,top: 10 ),
                          child:Text('Title: ${profile.firstname}',style:TextStyle(
                            fontSize: 30, color: Colors.white, fontWeight: FontWeight.w700
                          ),),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20,top:450 ),

                        child:Text('Location:  ${profile.lastname} ',style:TextStyle(
                            fontSize: 25, color: Colors.white, fontWeight: FontWeight.w700
                        ),),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20,top:500 ),
                          child:Text('Open-Closer: ${profile.email}',style:TextStyle(
                              fontSize: 20, color: Colors.white, fontWeight: FontWeight.w700
                          ),),),
                          Padding(
                              padding: const EdgeInsets.only(left: 20,top:550 ),
                          child:Text('Contact: ${profile.phone}',style:TextStyle(
                              fontSize: 15, color: Colors.white, fontWeight: FontWeight.w700
                          ),),),
                        ],
                      ),
                    );
                }).toList(),

                  );
            }),
      ),
    );
  }
}
