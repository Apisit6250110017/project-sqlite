import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AboutMantita extends StatefulWidget {
  const AboutMantita({Key? key}) : super(key: key);

  @override
  _AboutMantitaState createState() => _AboutMantitaState();
}

class _AboutMantitaState extends State<AboutMantita> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('About Mantita Rabeadee'),
      ),
      body: Center(
        child: Stack(
          children: [
            Image(
              width: 400,
              height: 800,
              fit: BoxFit.cover,
              image :AssetImage("assets/images/03.jpg",),



            ),
            Padding(
              padding: const EdgeInsets.only(left: 20,top: 10 ),
              child:Text('Mantita Rabeadee',style:TextStyle(
                  fontSize: 30, color: Colors.black, fontWeight: FontWeight.w700
              ),),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20,top:450 ),

              child:Text('ID Student : ',style:TextStyle(
                  fontSize: 25, color: Colors.white, fontWeight: FontWeight.w700
              ),),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20,top:500 ),
              child:Text('6250110008',style:TextStyle(
                  fontSize: 20, color: Colors.white, fontWeight: FontWeight.w700
              ),),),
            Padding(
              padding: const EdgeInsets.only(left: 20,top:550 ),
              child:Text('Information And Computer Management',style:TextStyle(
                  fontSize: 15, color: Colors.white, fontWeight: FontWeight.w700
              ),),),

          ],

        ),




      ),

    );

  }
}
